<?php
    ini_set('display_errors',1);
    ini_set('display_startup_errors',1);
    error_reporting(E_ALL);

    require_once "db_connect.php";//соединение с БД
    require_once "Publication.class.php";//родительский класс Publication
    require_once "News.class.php";//дочерный класс News от Publication
    require_once "Article.class.php";//дочерный класс Article от Publication
    require_once "PublicationsWriter.class.php";//класс PublicationsWriter

    try{
        $sql = 'SELECT id,type,title,introduction FROM publication';//вызов данных из таблици publication
        $results = $pdo->query($sql);
    }catch(PDOException $e){
        echo "Ошибка получения данных: ".$e->getMessage();
        exit();
    }

    $news = 'news';
    $article = 'article';

    $publications = array();
    foreach ($results as $result){//создание объектов класса PublicationsWriter
       $publications[] = new PublicationsWriter ($result['id'], $result['type'], $result['title'], $result['introduction']);
    }

?>

<!DOCTYPE html>
<html>
<!-- HEADER START -->
<head>
	<title>Homework #15</title>

	<meta charset="utf-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

</head>
<!-- HEADER END -->
<body>
<!-- CONTENT START -->
	<div style="height: 900px; margin: 20px">
		<h1>Homework #15</h1>
        <hr>
		<h2 class="panel-body text-center">Разделы</h2>
		<div style="float: left; margin: 20px; width: 40%" class="panel-body text-center"><!-- вывод краткого описания статей и новостей -->
            <h3>News</h3>
            <br>
            <?=PublicationsWriter::getShortPreview($publications, $news);?>
        </div>
        <div style="float: right; margin: 20px; width: 40%" class="panel-body text-center">
            <h3>Article</h3>
            <br>
            <?=PublicationsWriter::getShortPreview($publications, $article);?>
        </div>
    </div>
<!-- CONTENT END -->
<div id="footer" style="clear:both">
    <div class="panel panel-default" style="background-color: green">
        <div class="panel-body text-center">
            Shapovalov (c) 2017
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>
