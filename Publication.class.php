<?php
    abstract class Publication{
        protected $id;
        protected $type;
        protected $title;
        protected $introduction;
        protected $full_text;

        public function __construct($type, $title, $introduction, $full_text){
            $this->type = $type;
            $this->title = $title;
            $this->introduction = $introduction;
            $this->full_text = $full_text;
        }

        public static function Create($id, PDO $pdo){//метод фабрика
            try {
                $sql1 = 'SELECT * FROM publication WHERE id=:id';
                $stmt = $pdo->prepare($sql1);
                $stmt->bindValue(':id', $id);
                $stmt->execute();
                $result = $stmt->fetchObject();
            }catch(PDOException $e){
                echo "Ошибка получения данных: ".$e->getMessage();
                exit();
            }
            if(empty($result)){
                return null;
            }
            if($result->type == 'news'){
                $people = new News(
                    $result->type,
                    $result->title,
                    $result->introduction,
                    $result->full_text,
                    $result->source
                );
            }else if($result->type == 'article'){
                $people = new Article(
                    $result->type,
                    $result->title,
                    $result->introduction,
                    $result->full_text,
                    $result->author
                );
            }
            $people->setID($result->id);
            return $people;
        }

        public function setID($id){//метод установки id
            $this->id = $id;
         }

        public function getType(){
            return $this->type;
        }

        public function getTitle(){
            return $this->title;
        }

        public function getFullText(){
            return $this->full_text;
        }
        }