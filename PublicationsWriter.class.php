<?php
    class PublicationsWriter{
        protected $id;
        protected $type;
        protected $title;
        protected $introduction;

        public function __construct($id, $type, $title, $introduction){
            $this->id = $id;
            $this->type = $type;
            $this->title = $title;
            $this->introduction = $introduction;
        }

        public static function getShortPreview($publications, $type){//метод вывода краткого описания
            $look = "";
            if($type == 'news'){
                foreach($publications as $publication){
                    if($publication->type == 'news'){
                        $look .= '<h4>'.$publication->title.'</h4>';
                        $look .= '<span>'.$publication->introduction.'</span>';
                        $look .= "<br>";
                        $look .= '<a class="btn btn-medium active" href="full.text.php?id='.$publication->id.'" role="button">'.'Читать'.'</a>';
                    }
                }
            }
            if($type == 'article'){
                foreach($publications as $publication){
                    if($publication->type == 'article'){
                        $look .= '<h4>'.$publication->title.'</h4>';
                        $look .= '<span>'.$publication->introduction.'</span>';
                        $look .= "<br>";
                        $look .= '<a class="btn btn-medium active" href="full.text.php?id='.$publication->id.'" role="button">'.'Читать'.'</a>';
                    }
                }
            }
        return $look;
        }
}