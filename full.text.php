<?php
    ini_set('display_errors',1);
    ini_set('display_startup_errors',1);
    error_reporting(E_ALL);

    require_once "db_connect.php";//соединение с БД
    require_once "Publication.class.php";//родительский класс Publication
    require_once "News.class.php";//дочерный класс News от Publication
    require_once "Article.class.php";//дочерный класс Article от Publication

    $publication = array();
    if(isset($_GET['id'])){
        $publication = Publication::Create($_GET['id'], $pdo);//создание объекта класса Publication методом фабрика
    }
?>

<!DOCTYPE html>
<html>
<!-- HEADER START -->
<head>
    <title>Homework #15</title>

    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

</head>
<!-- HEADER END -->
<body>
<!-- CONTENT START -->
<div style="margin: 20px">
    <h1>Homework #15</h1>
    <div>
        <ul class="nav nav-tabs" role="tablist">
            <li><a href="index.php">Главная</a></li>
        </ul>
    </div>
    <h2 class="panel-body text-center"><?=$publication->getType();?></h2>
    <div style="padding-left: 380px; margin: 20px"><!-- вывод полного текста -->
        <div style="margin: 20px; width: 80%" class="panel-body text-center">
            <h3><?=$publication->getTitle()?></h3>
            <?=include_once $publication->getFullText();?>
            <br>
            <br>
            <?if($publication->getType() == 'news'){
                echo 'Источник: '.$publication->getSource();
            }elseif($publication->getType() == 'article'){
                echo 'Автор: '.$publication->getAuthor();
            };?>
        </div>
    </div>
</div>
<!-- CONTENT END -->
<div id="footer" style="clear:both">
    <div class="panel panel-default" style="background-color: green">
        <div class="panel-body text-center">
            Shapovalov (c) 2017
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>
